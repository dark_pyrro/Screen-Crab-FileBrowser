This will make it possible to view the contents of the Hak5 Screen Crab without the need to open the case and connect to the hardware serial port. Why do this? Not sure really, it's just a way to view stuff on the Crab with no hardware mods needed.

NOTE! You are about to use a binary executable file that is not controlled by me (or Hak5 either) which may impose a certain amount of risk. So, by following this instruction, you are doing it at your own risk. It might also void warranty since it's changing the product to some extent (read; it will no longer be an original "out of the box" Hak5 product anymore even though it should be possible to revert the changes made). You have been warned!


Start making sure the usual basic setup of the Crab have been made:

https://docs.hak5.org/screen-crab/getting-started/screen-crab-basics

https://docs.hak5.org/screen-crab/getting-started/configuring-the-screen-crab

Also make sure that WiFi is set up and that the Crab is able to connect successfully to the configured WLAN:

https://docs.hak5.org/screen-crab/getting-started/configuring-cloud-c

Download the Caddy web server from:

https://caddyserver.com/

Select "Linux arm 7" as platform when downloading, the `caddy_linux_arm7` binary should be downloaded

Copy the `caddy_linux_arm7` files along with the following files from this repository to the root of the Crab Micro SD card (use a card reader to copy the file to the Micro SD card): 
`Caddyfile`
`autoexec.txt`
`execute.sh`

The shell script file might seem like overkill, but the Caddy web server may have issues starting correctly on the Crab if including it in the one-liner in the `autoexec.txt` file.

Safely eject the Micro SD card from whatever device you have been using to copy the files mentioned above.

With the Crab powered off, insert the Micro SD card and then boot the Crab. Wait for the Crab to boot up (wait approx. 1 minute to be sure).

Open a web browser on any device that is connected to the same LAN as the Crab and open:

http://whatever-IP-address-the-Crab-has-on-the-WiFi-network/

You should now be able to browse the internal file system of the Crab that is mounted to "root" (/)

If having issues getting the web page/file listing to show, then make sure mandatory https isn't configured on the browser (or that the browser flips over to use https for some other reason). Also try to open the http-location in an incognito window. This way of accessing the Screen Crab isn't fail-safe and is not an official feature, thus unsupported.  


Added "tweak": Entered an initial 30 second sleep in the script file. If not present, the Crab might halt during boot. It needs time to finish the boot sequence before kicking off the shell script.
